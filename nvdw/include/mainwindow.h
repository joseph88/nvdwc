#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm.h>

#include "todayimgview.h"
#include "allimgview.h"
#include "nvipc.h"
#include "nvsettings.h"
#include "imgpackdb.h"
#include "packimgview.h"

class MainWindow : public Gtk::Window {
    public:
        /** Default constructor */
        MainWindow();
        /** Default destructor */
        virtual ~MainWindow();

        NvSettings sett;
        ImgPackDB db;
        NVExitStatus exitStatus() {
            ipc.read();
            return ipc.exitStatus();
        }
        bool isPaused() const {
            return ipc.isPaused();
        }
        void nextImage() {
            ipc.nextImage();
        }
        void togglePause() {
            ipc.setPaused(ipc.isPaused());
        }
        void toggleDaemon() {
            ipc.setExit();
        }
        void updateThumbSize(int newSize);

    protected:
        Gtk::Notebook tabs;
        TodayImgView today;
        AllImgView all;
        PackImgView pack;

        NvIpc ipc;
    private:
};

#endif // MAINWINDOW_H
