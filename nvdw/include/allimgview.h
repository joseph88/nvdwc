#ifndef ALLIMGVIEW_H
#define ALLIMGVIEW_H

#include "imgview.h"

class AllImgView : public ImgView {
    public:
        /** Default constructor */
        AllImgView(MainWindow& mainWindow);
        /** Default destructor */
        virtual ~AllImgView();

        void refresh();
        void setupVScroll();
    protected:
        void scrollValueChanged();
        void scrollChanged();
        double prevVal = 0.1;

    private:
};

#endif // ALLIMGVIEW_H
