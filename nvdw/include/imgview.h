#ifndef IMGVIEW_H
#define IMGVIEW_H

#include<gtkmm.h>
#include<string>
#include<vector>

#include "imgwidget.h"

class MainWindow;

class ImgView : public Gtk::Box {
    public:
        /** Default constructor */
        ImgView(MainWindow& mainWindow);
        /** Default destructor */
        virtual ~ImgView();

        Gtk::Label label;

        void refresh();

    protected:
        Gtk::Box ctrls;
        Gtk::FlowBox flow;
        Gtk::ScrolledWindow scroll;

        void refresh(const std::vector<Img> imgs);

        MainWindow& mainWindow;
    private:
};

#endif // IMGVIEW_H
