#ifndef IMGEDITDIALOG_H
#define IMGEDITDIALOG_H

#include <gtkmm.h>

class ImgEditDialog : Gtk::Dialog {
    public:
        /** Default constructor */
        ImgEditDialog();
        /** Default destructor */
        virtual ~ImgEditDialog();

    protected:

    private:
};

#endif // IMGEDITDIALOG_H
