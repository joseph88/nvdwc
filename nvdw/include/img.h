#ifndef IMG_H
#define IMG_H

#include <string>
#include <set>
#include <algorithm>

#include "uuid.h"

class Img {
    public:
        std::string name;
        std::string origFile;
        unsigned int id = 0;
        std::set<unsigned short> dates;
        unsigned char rate = 2;
        std::string uuid;

        static inline std::string EXT = ".jpg";

        Img();
        virtual ~Img();

        std::string toString() const;
        std::string debugString() const;
        bool isOnDoy(unsigned short doy) const {
            return (std::find(dates.begin(), dates.end(), doy) != dates.end());
        }
        short doyIndex() const {
            if (dates.empty()) return -1;
            else return *(dates.begin());
        }
        std::string getFirstDayString() const;

        bool toggleOnDay(unsigned short doy);

        void readVer1(std::ifstream& m);

        bool operator==(const Img& cimg) const;
        bool operator==(const unsigned int& cid) const;
        bool operator==(const std::string& cuuid) const;

        friend std::ofstream &operator<<(std::ofstream &m, const Img &i);
        friend std::ifstream &operator>>(std::ifstream &m, Img &i);

    protected:
    private:
};

#endif // IMG_H
