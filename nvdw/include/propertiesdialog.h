#ifndef PROPERTIESDIALOG_H
#define PROPERTIESDIALOG_H

#include <gtkmm.h>

class MainWindow;

class PropertiesDialog : public Gtk::Dialog {
    public:
        /** Default constructor */
        PropertiesDialog(MainWindow& mainWindow);
        /** Default destructor */
        virtual ~PropertiesDialog();
    protected:
        //signal handlers

        void on_thumb_size_changed();
        void on_switch_time_changed();
        void on_close_btn();

        //child widgets
        Gtk::RadioButton useGnome;
        Gtk::RadioButton useCustom;
        Gtk::HBox tbox;

        Glib::RefPtr<Gtk::Adjustment> thumbAdj;
        Glib::RefPtr<Gtk::Adjustment> switchTimeAdj;
        Glib::RefPtr<Gtk::Adjustment> checkTimebAdj;

        Gtk::Label thumbLabel;
        Gtk::SpinButton thumbSize;
        Gtk::Label switchTimeLabel;
        Gtk::SpinButton switchTime;
        Gtk::Label checkTimeLabel;
        Gtk::SpinButton checkTime;
        Gtk::Entry custEntry;
        Gtk::Label custLabel;

        Gtk::Button clsButton;
    private:
        MainWindow& mainWindow;
        bool needsSave{false};
        bool updateThumbs{false};
};

#endif // PROPERTIESDIALOG_H
