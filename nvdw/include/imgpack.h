#ifndef IMGPACK_H
#define IMGPACK_H

#include <string>
#include <vector>

#include "uuid.h"
#include "img.h"

class ImgPack {
    public:
        /** Default constructor */
        ImgPack();
        /** Default destructor */
        virtual ~ImgPack();

        std::string name;
        std::string descr;
        std::vector<Img> imgs;
        std::string uuid;

        Img& createNewImage(std::string srcPath, unsigned int id);

        unsigned int size() const {
            return imgs.size();
        }

        bool operator==(const ImgPack& cpack) const;
        bool operator==(const std::string& cuuid) const;

        friend std::ofstream &operator<<(std::ofstream &m, const ImgPack &i);
        friend std::ifstream &operator>>(std::ifstream &m, ImgPack &i);

        void readVer1(std::ifstream& m);

        std::string toString() const;
        std::string debugString() const;
        const std::pair<Img, bool> findImage(unsigned int id) const;

        //Img* getImageRef(unsigned int id);

    protected:
        const Img notFound;

    private:
};

#endif // IMGPACK_H
