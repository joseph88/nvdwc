#ifndef PACKMODEL_H
#define PACKMODEL_H

#include <gtkmm.h>
#include <string>

class PackModel : public Gtk::TreeModel::ColumnRecord {
    public:
        /** Default constructor */
        PackModel();
        /** Default destructor */
        virtual ~PackModel();

        Gtk::TreeModelColumn<std::string> col_uuid;
        Gtk::TreeModelColumn<std::string> col_name;

    protected:

    private:
};

#endif // PACKMODEL_H
