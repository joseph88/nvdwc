#ifndef PACKIMGVIEW_H
#define PACKIMGVIEW_H

#include "imgview.h"
#include "packmodel.h"

class MainWindow;

class PackImgView : public ImgView {
    public:
        /** Default constructor */
        PackImgView(MainWindow& mainWindow);
        /** Default destructor */
        virtual ~PackImgView();

        void refresh();
        void refreshImages();

    protected:
        PackModel packModel;
        Glib::RefPtr<Gtk::ListStore> listStore;
        Gtk::ComboBox packSelect;
        void onComboChanged();

    private:
};

#endif // PACKIMGVIEW_H
