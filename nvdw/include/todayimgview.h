#ifndef TODAYIMGVIEW_H
#define TODAYIMGVIEW_H

#include "imgview.h"
#include "nvtimer.h"

class MainWindow;

class TodayImgView : public ImgView {
    public:
        /** Default constructor */
        TodayImgView(MainWindow& mainWindow);
        /** Default destructor */
        virtual ~TodayImgView();

        void refresh();
    protected:
        Gtk::Button nextBtn;
        Gtk::Button ststBtn;
        Gtk::Button pauseBtn;
        Gtk::Button propertiesBtn;

        void onNextBtnClicked();
        void onStstBtnClicked();
        void onPauseBtnClicked();
        void onPropsBtnClicked();
        void updatePauseBtn();
        void updateStstBtn();

        void notify();
        void on_timer();

        Glib::Dispatcher dispatcher;
        //NvTimer timer;
};


#endif // TODAYIMGVIEW_H
