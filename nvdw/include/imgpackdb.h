#ifndef IMGPACKDB_H
#define IMGPACKDB_H

#include <vector>

#include "imgpack.h"
#include "imgpackdbidx.h"

class ImgPackDB {
    public:
        /** Default constructor */
        ImgPackDB(std::string fileName, std::string idxFileName = "");
        /** Default destructor */
        virtual ~ImgPackDB();

        static inline std::string const DEFAULT_FILENAME = "db.bin";
        const unsigned int MAGIC_NUMBER_OLD = 0x4E564457;
        const unsigned int MAGIC_NUMBER = 0x5744564E;
        const unsigned int FILE_VER = 2;

        void load();
        void save();
        void reindex();

        unsigned int imgCount() const;
        unsigned int size() const {
            return packs.size();
        }
        const std::pair<Img, bool> findImage(unsigned int) const;
        const std::vector<Img> getTodayImages() const;
        const std::vector<Img> getAllImages() const;
        const std::vector<Img> getPackImages(int pos) const;
        const std::vector<Img> getPackImages(std::string packUUID) const;
        const std::vector<std::pair<std::string, std::string>> getPackIds() const;
        static bool compare(Img a, Img b) {
            return a.doyIndex() < b.doyIndex();
        }

        void readVer1(std::ifstream& m);

        std::string debugString();
    protected:
        unsigned int nextId = 1;
        std::string filename;
        std::vector<ImgPack> packs;
        ImgPackDBIdx idx;

        friend std::ofstream &operator<<(std::ofstream &m, const ImgPackDB &i);
        friend std::ifstream &operator>>(std::ifstream &m, ImgPackDB &i);
    private:
};

#endif // IMGPACKDB_H
