#ifndef IMGWIDGET_H
#define IMGWIDGET_H

#include <gtkmm.h>
#include "img.h"

class MainWindow;

class ImgWidget : public Gtk::Box {
    public:
        /** Default constructor */
        ImgWidget(MainWindow& mainWindow, const Img& img);
        /** Default destructor */
        virtual ~ImgWidget();

    protected:
        unsigned int imgId;

        Gtk::Image image;
        Gtk::Label title;
        Gtk::Label day;

        MainWindow& mainWindow;
    private:
};

#endif // IMGWIDGET_H
