#include "allimgview.h"

#include <iostream>
#include "mainwindow.h"
#include "imgview.h"

AllImgView::AllImgView(MainWindow& mainWindow) : ImgView(mainWindow) {
    //ctor
    label.set_label("All Images");
    setupVScroll();
}

AllImgView::~AllImgView() {
    //dtor
}

void AllImgView::setupVScroll() {
    Glib::RefPtr<Gtk::Adjustment> vadjust = scroll.get_vadjustment();
    vadjust->signal_value_changed().connect(sigc::mem_fun(*this, &AllImgView::scrollValueChanged));
    vadjust->signal_changed().connect(sigc::mem_fun(*this, &AllImgView::scrollChanged));
    prevVal = mainWindow.sett.allPos;
}

void AllImgView::refresh() {
    ImgView::refresh(mainWindow.db.getAllImages());
}

/*  The adjustment can be reset such that the value is set to 0, not sure if this is a GTK bug or not
 *  but its really annoying.  This bit of logic keeps our scroll position even if the adjustment is
 *  re-initialized back to 0.  Only allow a 0 reset if we were close to 0 anyway.
 */
void AllImgView::scrollValueChanged() {
    Glib::RefPtr<Gtk::Adjustment> vadjust = scroll.get_vadjustment();
    double val = vadjust->get_value();
    if(val > 0.0 || prevVal < 200) {
        prevVal = mainWindow.sett.allPos;
        mainWindow.sett.allPos = val;
        mainWindow.sett.save();
    } else {
        vadjust->set_value(prevVal);
    }
}

void AllImgView::scrollChanged() {
    Glib::RefPtr<Gtk::Adjustment> vadjust = scroll.get_vadjustment();
    if(mainWindow.sett.allPos > 0) {
        vadjust->set_value(mainWindow.sett.allPos);
    }
}
