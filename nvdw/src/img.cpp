#include <fstream>
#include <iostream>

#include "img.h"
#include "util.h"
#include "nvuuid.h"

using namespace std;

Img::Img() {
    //ctor
    uuid = newUUID();
}

Img::~Img() {
    //dtor
}

string Img::toString() const {
    string s = "";
    s += to_string(id);
    s += ":";
    s += name;
    s += ":";
    s += uuid;
    s += ":";
    s += to_string((short)rate);
    return s;
}


string Img::debugString() const {
    string s = toString();
    s += "[";
    for(auto d: dates) {
        s += to_string(d);
        s += ",";
    }
    s += "]";
    return s;
}

bool Img::toggleOnDay(unsigned short doy) {
    for(auto d = dates.begin(); d != dates.end(); ++d) {
        if(*d == doy) {
            dates.erase(d);
            return false;
        }
    }
    dates.insert(doy);
    return true;
}

string Img::getFirstDayString() const {
    if(dates.empty()) {
        return "not set";
    } else {
        return getDoyDate(*dates.begin());
    }
}

bool Img::operator==(const Img& cimg) const {
    return id == cimg.id;
}

bool Img::operator==(const unsigned int& cid) const {
    return id == cid;
}

bool Img::operator==(const string& cuuid) const {
    return uuid == cuuid;
}

std::ofstream &operator<<(std::ofstream &m, const Img &i) {
    writeUInt(m, i.id);
    writeUChar(m, i.rate);
    writeSString(m, i.uuid);
    writeSString(m, i.name);
    writeSString(m, i.origFile);
    unsigned short sz = i.dates.size();
    writeUShort(m, sz);
    for(auto d: i.dates) {
        writeUShort(m, d);
    }
    return m;
}

std::ifstream &operator>>(std::ifstream &m, Img &i) {
    i.id = readUInt(m);
    i.rate = readUChar(m);
    i.uuid = readSString(m);
    i.name = readSString(m);
    i.origFile = readSString(m);
    i.dates.clear();
    unsigned short sz = readUShort(m);
    unsigned short d;
    for(auto x = 0; x < sz; x++) {
        d = readUShort(m);
        i.dates.insert(d);
    }
    return m;
}

void Img::readVer1(ifstream& m) {
    id = readIntVB(m);
    if(id < 1) throw "Invalid Img ID ";
    rate = readIntVB(m);
    if(rate < 0) throw "Invalid Img rating ";
    uuid = readStringVB(m);
    name = readStringVB(m);
    origFile = readStringVB(m);
    unsigned int cnt = readIntVB(m);
    dates.clear();
    unsigned short d = 0;
    for(auto i = 0; i < cnt; i++) {
        d = readIntVB(m);
        dates.insert(d);
    }
}
