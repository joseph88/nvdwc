#include "imgwidget.h"

#include "util.h"
#include "mainwindow.h"

#include <iostream>

using namespace Gtk;
using namespace std;

ImgWidget::ImgWidget(MainWindow& mainWindow, const Img& img) : mainWindow(mainWindow) {
    //ctor
    set_orientation(ORIENTATION_VERTICAL);
    imgId = img.id;

    int w = mainWindow.sett.thumbSize;
    int h = w * .75;
    Glib::RefPtr<Gdk::Pixbuf> pixb = Gdk::Pixbuf::create_from_file(mainWindow.sett.getThumbFilePath(img.id).c_str(), w, h);
    image.set(pixb);

    title.set_text(img.name);
    day.set_text(img.getFirstDayString());
    add(image);
    add(title);
    add(day);
}

ImgWidget::~ImgWidget() {
    //dtor
}
