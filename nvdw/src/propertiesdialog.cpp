#include "propertiesdialog.h"

#include <iostream>
#include <cstdio>

#include "mainwindow.h"

PropertiesDialog::PropertiesDialog(MainWindow& mainWindow) : mainWindow(mainWindow) {
    //ctor
    thumbLabel.set_text("Thumbnail Size: ");
    tbox.add(thumbLabel);

    thumbAdj = Gtk::Adjustment::create(mainWindow.sett.thumbSize, 32.0, 512.0, 8.0, 0.0, 0.0);
    thumbSize.set_editable(true);
    thumbSize.set_adjustment(thumbAdj);
    thumbSize.signal_value_changed().connect(sigc::mem_fun(*this, &PropertiesDialog::on_thumb_size_changed));
    tbox.add(thumbSize);

    switchTimeLabel.set_text("Switch Time: ");
    tbox.add(switchTimeLabel);

    switchTimeAdj = Gtk::Adjustment::create(mainWindow.sett.switchSeconds, 5.0, 10000.0, 5.0, 0.0, 0.0);
    switchTime.set_editable(true);
    switchTime.set_adjustment(switchTimeAdj);
    switchTime.signal_value_changed().connect(sigc::mem_fun(*this, &PropertiesDialog::on_switch_time_changed));
    tbox.add(switchTime);

    tbox.show_all();
    auto boxen = get_content_area();
    boxen->pack_start(tbox);

    //clsButton
    clsButton = Gtk::Button("close");
    clsButton.signal_clicked().connect(sigc::mem_fun(*this, &PropertiesDialog::on_close_btn));

    boxen->pack_end(clsButton);

    show_all();

    set_title("Properties");
}

PropertiesDialog::~PropertiesDialog() {
    //dtor
}

void PropertiesDialog::on_switch_time_changed() {
    mainWindow.sett.switchSeconds = switchTime.get_value_as_int();
    needsSave = true;
}

void PropertiesDialog::on_thumb_size_changed() {
    updateThumbs = true;
    needsSave = true;
}

void PropertiesDialog::on_close_btn() {

    if(updateThumbs) {
        mainWindow.updateThumbSize(thumbSize.get_value_as_int());
    }

    if(needsSave) {
        mainWindow.sett.save();
    }

    this->close();
}
