#include <filesystem>
#include <iostream>

#include "util.h"
#include "imgpack.h"
#include "nvuuid.h"

using namespace std;

ImgPack::ImgPack() {
    //ctor
    uuid = newUUID();
}

ImgPack::~ImgPack() {
    //dtor
}


Img& ImgPack::createNewImage(string srcPath, unsigned int id) {
    filesystem::path pth = filesystem::path(srcPath);

    Img i;
    i.origFile =  pth.filename();
    int ppos = i.origFile.find('.') ;
    i.name = i.origFile.substr(0, ppos);
    i.id = id;
    imgs.push_back(i);
    return imgs.back();
}

bool ImgPack::operator==(const ImgPack& cpack) const {
    return uuid == cpack.uuid;
}

bool ImgPack::operator==(const string& cuuid) const {
    return uuid == cuuid;
}

std::ofstream &operator<<(std::ofstream &m, const ImgPack &i) {
    writeSString(m, i.uuid);
    writeSString(m, i.name);
    writeSString(m, i.descr);
    unsigned short sz = i.imgs.size();
    writeUShort(m, sz);
    for(auto img: i.imgs) {
        m << img;
    }
    return m;
}

std::ifstream &operator>>(std::ifstream &m, ImgPack &i) {
    i.uuid = readSString(m);
    i.name = readSString(m);
    i.descr = readSString(m);
    unsigned short sz = readUShort(m);
    Img img;
    i.imgs.clear();
    for(auto x = 0; x < sz; x++) {
        m >> img;
        i.imgs.push_back(img);
    }
    return m;
}

void ImgPack::readVer1(ifstream& m) {
    try {
        name = readStringVB(m);
        descr = readStringVB(m);
        uuid = readStringVB(m);
        unsigned int sz = readIntVB(m);
        imgs.clear();
        Img i;
        for(int x = 0; x < sz; x++) {
            i.readVer1(m);
            imgs.push_back(i);
        }
    } catch (const exception &exc) {
        cerr << "Oh Fudge!! >> " << exc.what() << endl << endl;
    }
}

string ImgPack::toString() const {
    return name;
}

const pair<Img, bool> ImgPack::findImage(unsigned int id) const {
    Img i;
    if(imgs.empty()) return pair(i, false);
    for(auto itr = imgs.begin(); itr < imgs.end(); itr++) {
        if((*itr).id == id) return pair(*itr, true);
    }
    return pair(i, false);
}

string ImgPack::debugString() const {
    string s = name;
    s += " (";
    s += to_string(imgs.size());
    s += " ) : ";
    s += descr;
    s += " : ";
    s += uuid;
    s += " \n---------------------------------------------\n";
    for(auto i: imgs) {
        s += "   ";
        s += i.debugString();
        s += "\n";
    }
    return s;
}
