#include "mainwindow.h"

#include <iostream>

using namespace std;

MainWindow::MainWindow() :
    db(sett.getDataFilePath(ImgPackDB::DEFAULT_FILENAME)),
    ipc(sett.getDataFilePath(NvIpc::DEFAULT_FILENAME)),
    today(*this),
    all(*this),
    pack(*this) {
    //ctor
    sett.load();

    // also check out set_geometry_hints
    resize((int)sett.screenWidth, (int)sett.screenHeight);

    tabs.append_page(today, today.label);
    tabs.append_page(all, all.label);
    tabs.append_page(pack, pack.label);
    add(tabs);
    show_all();
    db.load();

    ipc.read();

    pack.refresh();
    all.refresh();
    today.refresh();
}

MainWindow::~MainWindow() {
    //dtor
    int w, h;
    get_size(w, h);

    sett.screenWidth = w;
    sett.screenHeight = h;

    sett.save();
}

void MainWindow::updateThumbSize(int newSize) {
    sett.thumbSize = newSize;
    today.refresh();
    all.refresh();
    pack.refresh();
}
