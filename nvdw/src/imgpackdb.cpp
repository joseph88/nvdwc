#include <iostream>
#include <fstream>
#include <filesystem>

#include "imgpackdb.h"
#include "util.h"

using namespace std;

ImgPackDB::ImgPackDB(string fileName, string idxFileName) {
    //ctor
    nextId = 1;
    this->filename = fileName;
    if(this->filename.empty()) this->filename = makeDataPath(DEFAULT_FILENAME);
    string ifn = idxFileName;
    if(ifn.empty()) {
        filesystem::path p(filename);
        ifn = p.parent_path();
        ifn += "/";
        ifn += ImgPackDBIdx::DEFAULT_FILENAME;
    }
    idx.setFilename(ifn);
}

ImgPackDB::~ImgPackDB() {
    //dtor
}

void ImgPackDB::load() {
    ifstream m(filename, ios::in | ios::binary);
    m >> *this;
    m.close();
    idx.load();
    if(idx.imgCount() != imgCount()) {
        reindex();
        idx.save();
    }
}

void ImgPackDB::save() {
    ofstream m(filename, ios::out | ios::binary);
    m << *this;
    m.close();
    idx.save();
}

void ImgPackDB::reindex() {
    idx.clear();
    for(auto p: packs) {
        for(auto img: p.imgs) {
            for(auto doy: img.dates) {
                idx.updateDoy(img.id, doy);
            }
        }
    }
}

const pair<Img, bool> ImgPackDB::findImage(unsigned int id) const {
    Img nothing;
    if(packs.empty()) return pair(nothing, false);
    for(auto p: packs) {
        //cout << " ipdb Looking for " << id << " " << p.name;
        auto r = p.findImage(id);
        if(r.second) return r;
    }
    return pair(nothing, false);
}

const vector<Img> ImgPackDB::getTodayImages() const {
    vector<Img> v;
    set<unsigned int> ids = idx.readForDoy(getDoy());
    v.reserve(ids.size());
    Img i;
    for(auto id: ids) {
        //cout << " Looking for " << id << endl;
        auto [img, found] = findImage(id);
        //cout << " found " << found << " " << img.name << endl;
        if(found) v.push_back(img);
    }
    sort(v.begin(), v.end(), ImgPackDB::compare);
    return v;
}

const vector<Img> ImgPackDB::getAllImages() const {
    vector<Img> v;
    v.reserve(imgCount());
    Img i;
    for(auto p: packs) {
        v.insert(v.end(), p.imgs.begin(), p.imgs.end());
    }
    sort(v.begin(), v.end(), ImgPackDB::compare);
    return v;
}

const vector<Img> ImgPackDB::getPackImages(int pos) const {
    return packs.at(pos).imgs;
}


const vector<Img> ImgPackDB::getPackImages(std::string packUUID) const {
    return find(packs.begin(), packs.end(), packUUID)->imgs;
}

const vector<pair<string, string>> ImgPackDB::getPackIds() const {
    vector<pair<string, string>> pids;

    for (auto p: packs) {
        pids.push_back(pair(p.name, p.uuid));
    }

    return pids;
}

unsigned int ImgPackDB::imgCount() const {
    unsigned int cnt = 0;
    for(auto pck: packs) {
        cnt += pck.size();
    }
    return cnt;
}


void ImgPackDB::readVer1(ifstream& m) {
    unsigned int magic = readIntVB(m);
    cout << magic << " :: " << MAGIC_NUMBER_OLD << endl;
    unsigned int fv = readIntVB(m);
    cout << " fv: " << fv << endl;
    unsigned int nextId = readIntVB(m);
    cout << " nextId: " << nextId << endl;
    unsigned int cnt = readIntVB(m);
    cout << " cnt: " << cnt << endl;

    packs.clear();
    ImgPack ip;
    for(auto x = 0; x < cnt; x++) {
        ip.readVer1(m);
        packs.push_back(ip);
    }
}

string ImgPackDB::debugString() {
    string s = "\n==========  ";
    s += to_string(packs.size());
    s +=  " Packs  ==============\n";
    for(auto p: packs) {
        s += p.debugString();
    }
    s += "\n==========================\n";
    return s;
}

std::ofstream &operator<<(std::ofstream &m, const ImgPackDB &i) {
    writeUInt(m, i.MAGIC_NUMBER);
    writeUShort(m, i.FILE_VER);
    writeUInt(m, i.nextId);
    unsigned short sz = i.packs.size();
    writeUShort(m, sz);
    for(ImgPack pack: i.packs) {
        m << pack;
    }
    return m;
}

std::ifstream &operator>>(std::ifstream &m, ImgPackDB &i) {
    unsigned int magic = readUInt(m);
    //cout << " magic " << magic << endl;
    unsigned short ver = readUShort(m);
    //cout << " ver " << ver << endl;
    i.nextId = readUInt(m);
    //cout << " nextId " << i.nextId << endl;
    unsigned int sz = readUShort(m);
    //cout << " sz " << sz << endl;

    i.packs.clear();
    ImgPack pack;
    for(auto x = 0; x < sz; x++) {
        m >> pack;
        i.packs.push_back(pack);
    }
    return m;
}
