#include "packimgview.h"

#include "mainwindow.h"
#include "packmodel.h"

#include <iostream>

using namespace std;

PackImgView::PackImgView(MainWindow& mainWindow) : ImgView(mainWindow) {
    //ctor
    label.set_label("Image Packs");
    listStore = Gtk::ListStore::create(packModel);
    packSelect.set_model(listStore);
    packSelect.pack_start(packModel.col_name);
    packSelect.signal_changed().connect(sigc::mem_fun(*this, &PackImgView::onComboChanged));
    ctrls.add(packSelect);
}

PackImgView::~PackImgView() {
    //dtor
}

void PackImgView::refresh() {
    listStore->clear();
    if(mainWindow.db.size() > 0) {
        Gtk::TreeModel::iterator iter;
        for (auto [name, uuid]: mainWindow.db.getPackIds()) {
            iter = listStore->append();
            (*iter)[packModel.col_name] = name;
            (*iter)[packModel.col_uuid] = uuid;
        }
        packSelect.set_active(listStore->children().begin());
    }
}

void PackImgView::refreshImages() {
    auto itr = packSelect.get_active();
    if(itr) {
        auto row = *itr;
        if(row) {
            string uuid = row[packModel.col_uuid];
            ImgView::refresh(mainWindow.db.getPackImages(uuid));
        }
    }
}

void PackImgView::onComboChanged() {
    refreshImages();
}
