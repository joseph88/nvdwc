#include "imgview.h"

#include "mainwindow.h"
#include  <iostream>

using namespace std;

ImgView::ImgView(MainWindow& mainWindow) : mainWindow(mainWindow) {
    //ctor
    this->set_orientation(Gtk::ORIENTATION_VERTICAL);
    ctrls.set_orientation(Gtk::ORIENTATION_HORIZONTAL);

    Glib::ustring css = " flowbox{ background: black; } flowboxchild { background: black; padding: 0px; margin: 0px 0px 0px 0px; } flowboxchild image { padding: 0px; margin: 0px -10px 0px -10px; }  flowboxchild label { padding: 0px; margin: 0px -10px 0px -10px; color: #aaaaff; }";
    Glib::RefPtr<Gtk::CssProvider> prov = Gtk::CssProvider::create();
    prov->load_from_data(css);

    this->get_style_context()->add_provider_for_screen(this->get_screen(), prov, 0);

    scroll.add(flow);
    pack_start(ctrls, Gtk::PACK_SHRINK);
    pack_end(scroll);
}

ImgView::~ImgView() {
    //dtor
}

void ImgView::refresh(const vector<Img> imgs) {

    for(auto c: flow.get_children()) {
        flow.remove(*c);
    }

    //cout << "Images: " << imgs.size() << endl;
    for(auto img: imgs) {
        auto iw = Gtk::make_managed<ImgWidget>(mainWindow, img);
        if(iw == nullptr) {
            cout << "Could not create iw" << endl;
        } else  {
            flow.add(*iw);
            //cout << "making image " << img.name << endl;
        }
    }
    flow.show_all();
}
