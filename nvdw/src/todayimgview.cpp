#include "todayimgview.h"

#include <gtkmm.h>
#include <vector>
#include <iostream>
#include <thread>

#include "imgview.h"
#include "mainwindow.h"
#include "propertiesdialog.h"

using namespace std;

TodayImgView::TodayImgView(MainWindow& mainWindow) : ImgView(mainWindow),
    nextBtn("Next"),
    ststBtn("Stop"),
    pauseBtn("Pause"),
    propertiesBtn("Properties") {
    //ctor

    label.set_label("Today");

    nextBtn.signal_clicked().connect( sigc::mem_fun(*this, &TodayImgView::onNextBtnClicked));
    ststBtn.signal_clicked().connect( sigc::mem_fun(*this, &TodayImgView::onStstBtnClicked));
    pauseBtn.signal_clicked().connect( sigc::mem_fun(*this, &TodayImgView::onPauseBtnClicked));
    propertiesBtn.signal_clicked().connect( sigc::mem_fun(*this, &TodayImgView::onPropsBtnClicked));

    dispatcher.connect(sigc::mem_fun(*this, &TodayImgView::on_timer));

    ctrls.pack_start(nextBtn, Gtk::PACK_SHRINK);
    ctrls.add(ststBtn);
    ctrls.add(pauseBtn);
    ctrls.add(propertiesBtn);
}

TodayImgView::~TodayImgView() {
    //dtor
}

void TodayImgView::refresh() {
    ImgView::refresh(mainWindow.db.getTodayImages());
    updatePauseBtn();
    updateStstBtn();
}

void TodayImgView::onNextBtnClicked() {
    mainWindow.nextImage();
}

void TodayImgView::onStstBtnClicked() {
    mainWindow.toggleDaemon();
    updateStstBtn();
    thread t([=]() {
        this_thread::sleep_for(chrono::seconds(2));
        dispatcher.emit();
    });

    t.detach();
}

void TodayImgView::onPauseBtnClicked() {
    mainWindow.togglePause();
    updatePauseBtn();
}

void TodayImgView::onPropsBtnClicked() {
    PropertiesDialog pd(mainWindow);
    pd.run();
}

void TodayImgView::updatePauseBtn() {
    if(mainWindow.isPaused()) {
        pauseBtn.set_label("Play");
    } else {
        pauseBtn.set_label("Pause");
    }
}

void TodayImgView::updateStstBtn() {
    switch(mainWindow.exitStatus()) {
    case NVExitStatus::EXIT:
        ststBtn.set_label("Stopping...");
        break;
    case NVExitStatus::EXITED:
        ststBtn.set_label("Start");
        break;
    case NVExitStatus::RUNNING:
        ststBtn.set_label("Stop");
        break;
    case NVExitStatus::UNKNOWN:
        ststBtn.set_label("Start?");
        break;
    }
}

void TodayImgView::on_timer() {
    updateStstBtn();
}
