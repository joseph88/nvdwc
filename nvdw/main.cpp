#include <iostream>
#include <fstream>
#include <glibmm/i18n.h>
#include <gtkmm.h>
#include <filesystem>

#include "imgpackdb.h"
#include "mainwindow.h"
#include "nvsettings.h"


using namespace std;
using namespace Gtk;

int main (int argc, char *argv[]) {
    auto app = Gtk::Application::create(argc, argv, "com.novisso.nvdw");
    MainWindow window;

    //filesystem::path p("/home/joseph/var/temp/db.bin");

    NvSettings sett;
    sett.load();
    ImgPackDB db(sett.getDataFilePath(ImgPackDB::DEFAULT_FILENAME));
    db.load();

    /*
    FlowBox box;
    ScrolledWindow sw;

    sw.add(box);

    ImgPackDB ipdb;

    ifstream m2( "/home/joseph/var/temp/nvdw8/data/db2.bin", ios::in | ios::binary );
    //ipdb.readVer1(m2);
    m2 >> ipdb;
    m2.close();

    //vector v<Image*>;

    for(ImgPack ip: ipdb.packs) {
        for (Img i: ip.imgs){
            string s = "/home/joseph/.config/nvdw8/data/thumbs/";
            s += to_string(i.id);
            s += ".jpg";
            Image* pi = make_managed<Image>(s);
            box.add(*pi);
        }
    }

    //ofstream m( "/home/joseph/var/temp/nvdw8/data/db2.bin", ios::out | ios::binary );
    //m << ipdb;
    //m.close();

    //buff->set_text(ipdb.debugString());

    window.add(sw);
    window.show_all_children();
    */
    return app->run(window);

    return 0;
}
