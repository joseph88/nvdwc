#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <chrono>
#include <string>

std::chrono::time_point nextSwitch = std::chrono::system_clock::now();
std::chrono::time_point nextCheck = std::chrono::system_clock::now();

void nvSwitch(NvIpc &ipc);
void nvSwitchTo(NvIpc &ipc, unsigned int id);
void nvCheckAndRotate(NvIpc &ipc);

void switchGnome(std::string filePath);

#endif // MAIN_H_INCLUDED
