#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <filesystem>


#include "imgpackdbidx.h"
#include "nvsettings.h"
#include "nvipc.h"
#include "main.h"
#include "util.h"

using namespace std;

NvSettings sett;
string dataDir;

int main()
{

    sett.load();
    dataDir = sett.dataDir;
    if(dataDir.length() == 0){
        dataDir = "/home/joseph/var/temp/";
    } else if(dataDir.back() != '/') {
        dataDir += "/";
    }

    NvIpc ipc(dataDir + NvIpc::DEFAULT_FILENAME);
    ipc.read();

    if(ipc.exitStatus() != NVExitStatus::EXIT) {
        ipc.setRunning();
        ipc.ipcRead();
        ipc.write();
        //int tries = 75;
        while ( ipc.exitStatus() == NVExitStatus::RUNNING ) {
            ipc.read();
            ipc.ipcRead();
            IpcCommand cmd = ipc.pop();
            if(cmd.cmd != NVCommand::NOP){
                cout << " cmd: " << static_cast<int>(cmd.cmd) << " : " << cmd.id << endl;
                switch(cmd.cmd) {
                    case NVCommand::SWITCH_NOW:
                        nvSwitch(ipc);
                        break;
                    case NVCommand::SWITCH_TO:
                        nvSwitchTo(ipc, cmd.id);
                        break;
                    case NVCommand::NOP: break;
                }
            }

            if(nextSwitch < chrono::system_clock::now()) {
                if(!ipc.isPaused()) nvSwitch(ipc);
            }
            if(nextCheck < chrono::system_clock::now()) {
                nvCheckAndRotate(ipc);
            }

            ipc.write();
            this_thread::sleep_for(chrono::milliseconds(sett.ipcMillis));

            //nuke these lines...
            //tries--;
            //if(tries <= 0) ipc.setExited();
        }
        cout << "ipc exited " << static_cast<int>(ipc.exitStatus()) << endl;
    }
    ipc.setExited();
    ipc.write();
    return 0;
}

void nvSwitch(NvIpc& ipc){
    nextSwitch = chrono::system_clock::now() + chrono::seconds(sett.switchSeconds);
    ImgPackDBIdx idx(dataDir + ImgPackDBIdx::DEFAULT_FILENAME);
    unsigned int lpid = ipc.currImg();
    auto idxs = idx.readForDoy(getDoy());
    if(!idxs.empty()) {
        auto itr = idxs.find(lpid);
        itr++;
        if(itr == idxs.end())
            itr = idxs.begin();
        ipc.updateSwitched(*itr);
        switchGnome(sett.getCurrFilePath(*itr));
        cout << "Do Switch: " << ipc.currImg()  << endl;
    } else {
        cout << "Nothing to show for today"<< endl;
    }
}


void nvSwitchTo(NvIpc& ipc, unsigned int id){
    nextSwitch += chrono::seconds(sett.switchSeconds);
    ipc.updateSwitched(id);
    switchGnome(sett.getCurrFilePath(id));
    cout << "Do Switch to " << id << endl;
}

void nvCheckAndRotate(NvIpc& ipc){
    nextCheck += chrono::seconds(sett.checkSeconds);
    ImgPackDBIdx idx(dataDir + ImgPackDBIdx::DEFAULT_FILENAME);

    string currPath = sett.getCurrPath();
    if(!filesystem::exists(filesystem::path(currPath))) {
        filesystem::create_directories(filesystem::path(currPath));
    }

    set<string> oldies;
    for (const auto & entry : filesystem::directory_iterator(sett.getCurrPath()))
        oldies.insert(entry.path().stem());

    auto idxs = idx.readForDoy(getDoy());
    if(!idxs.empty()) {
        set<string>::iterator itr;
        for(auto id: idxs){
            itr = oldies.find(to_string(id));
            if(itr != oldies.end()){
                oldies.erase(itr);
            }else {
                filesystem::copy(filesystem::path(sett.getImgFilePath(id)), filesystem::path(sett.getCurrFilePath(id)));
            }
        }
        for(auto str: oldies) {
            string opth = currPath;
            opth += str;
            opth += sett.DEFAULT_IMAGE_EXT;
            filesystem::remove(filesystem::path(opth));
        }
        ipc.updateChecked();
        cout << "Do Check " << endl;
    } else {
        cout << "No images "<< endl;
    }
}


void switchGnome(std::string filePath) {
    //desktop
    string cmd = "gsettings set org.gnome.desktop.background picture-uri ";
    cmd += filePath;
    int r = system(cmd.c_str());
    if(r != 0) {
        cout << "Error calling gsettings background " << r << endl;
    }
    //lock screen
    cmd = "gsettings set org.gnome.desktop.screensaver picture-uri ";
    cmd += filePath;
    r = system(cmd.c_str());
    if(r != 0) {
        cout << "Error calling gsettings screensaver " << r << endl;
    }
}
