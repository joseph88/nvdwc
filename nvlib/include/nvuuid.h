#ifndef UUID_HPP_INCLUDED
#define UUID_HPP_INCLUDED

#include <string>

std::string newUUID();

#endif // UUID_HPP_INCLUDED
