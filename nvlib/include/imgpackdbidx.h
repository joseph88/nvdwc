#ifndef IMGPACKDBIDX_H
#define IMGPACKDBIDX_H

#include <array>
#include <set>
#include <string>

class ImgPackDBIdx {
    public:
        /** Default constructor */
        ImgPackDBIdx();
        ImgPackDBIdx(std::string fileName);
        /** Default destructor */
        virtual ~ImgPackDBIdx();

        static inline std::string const DEFAULT_FILENAME = "db.idx";

        void load();
        void save();
        void setFilename(std::string fileName);
        unsigned int imgCount();
        void clear();
        void updateDoy(unsigned int iid, unsigned short doy);

        const std::set<unsigned int> readForDoy(unsigned int doy) const;

        std::string debugString();
    protected:
        std::string filename;
        std::array<std::set<unsigned int>, 366> days;
        friend std::ofstream &operator<<(std::ofstream &m, const ImgPackDBIdx &i);
        friend std::ifstream &operator>>(std::ifstream &m, ImgPackDBIdx &i);

    private:
};

#endif // IMGPACKDBIDX_H
