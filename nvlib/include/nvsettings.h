#ifndef NVSETTINGS_H
#define NVSETTINGS_H

#include <string>

class NvSettings {
    public:
        /** Default constructor */
        NvSettings();
        /** Default destructor */
        virtual ~NvSettings();

        static inline std::string const GNOME = "gnome";

        static inline std::string const DEFAULT_FILENAME = "settings.json";
        static inline std::string const DEFAULT_IMAGE_EXT = ".jpg";

        unsigned int switchSeconds = 60;
        unsigned int checkSeconds = 300;
        unsigned int ipcMillis = 400;
        std::string dataDir;
        std::string desktop;
        unsigned int allPos = 0;
        unsigned int thumbSize = 120;
        unsigned int screenWidth = 960;
        unsigned int screenHeight = 800;

        void save();
        void load();

        std::string getDataFilePath(std::string fileName);
        std::string getThumbFilePath(unsigned int id);
        std::string getImgFilePath(unsigned int id);
        std::string getCurrPath();
        std::string getCurrFilePath(unsigned int id);

        std::string debugString();

    protected:
        std::string filename;

    private:
};

#endif // NVSETTINGS_H
