#ifndef NVIPC_H
#define NVIPC_H

#include <fstream>
#include <queue>

enum class NVCommand {
    NOP, SWITCH_NOW, SWITCH_TO
};

class IpcCommand {
    public:
        IpcCommand();
        IpcCommand(NVCommand cmd, unsigned int id = 0);
        NVCommand cmd;
        unsigned int id;
};

enum class NVExitStatus {
    UNKNOWN, RUNNING, EXIT, EXITED
};

class NvIpc {
    public:
        const unsigned short MAX_CMD_COUNT = 300;

        /** Default constructor */
        NvIpc(std::string fileName);
        /** Default destructor */
        virtual ~NvIpc();

        static inline std::string const DEFAULT_FILENAME = "ipc.bin";

        void read();
        void write();
        void push(IpcCommand cmd);
        IpcCommand pop();
        long checked() const {
            return mChecked;
        }
        void updateChecked();
        long ipcRead() const {
            return mIpcread;
        }
        void updateIpcRead();
        long switched() const {
            return mSwitched;
        }
        void updateSwitched(unsigned int imageId);
        unsigned int currImg() const {
            return mCurrImgId;
        }
        NVExitStatus exitStatus() const {
            return mExitStatus;
        }
        void setRunning();
        void setExit();
        void setExited();
        void nextImage();
        bool isPaused() const {
            return mPaused == 1;
        }
        void setPaused(bool paused);

        std::string debugString();

    protected:
        std::string filename;
        NVExitStatus mExitStatus = NVExitStatus::UNKNOWN;
        unsigned long mIpcread = 0;
        unsigned long mChecked = 0;
        unsigned long mSwitched = 0;
        unsigned int mCurrImgId = 0;
        unsigned char mPaused = 0;
        std::deque<IpcCommand> dq;
    private:
};

#endif // NVIPC_H
