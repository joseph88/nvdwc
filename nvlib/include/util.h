#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <iostream>
#include <fstream>
#include <string>

#define MAX_SSTRING  32000
#define APP_ID  "nvdw8"

void writeUChar(std::ofstream &m, const unsigned char& s);
unsigned char readUChar(std::ifstream &m);

void writeUShort(std::ofstream &m, const unsigned short& s);
unsigned short readUShort(std::ifstream &m);

void writeUInt(std::ofstream &m, const unsigned int& i);
unsigned int readUInt(std::ifstream &m);

void writeSString(std::ofstream &m, const std::string& s);
std::string readSString(std::ifstream &m);

void writeULong(std::ofstream &m, const unsigned long& i);
unsigned long readULong(std::ifstream &m);

int readIntVB(std::ifstream& m);
long readLongVB(std::ifstream& m);
std::string readStringVB(std::ifstream& m);

unsigned int getDoy();

std::string makeDataPath(std::string fileName);
std::string getDefaultDataPath();

inline bool ends_with(std::string const & value, std::string const & ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}


std::string getDoyMonth(unsigned int doy);
unsigned short getDoyDay(unsigned int doy);

std::string getDoyDate(unsigned int doy);

#endif // UTIL_H_INCLUDED
