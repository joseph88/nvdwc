#include <fstream>
#include <chrono>

#include "nvipc.h"
#include "util.h"

using namespace std;
using namespace std::chrono;


IpcCommand::IpcCommand() {
    this->cmd = NVCommand::NOP;
    this->id = 0;
}

IpcCommand::IpcCommand(NVCommand cmd, unsigned int id) {
    this->cmd = cmd;
    this->id = id;
}

NvIpc::NvIpc(string fileName) {
    //ctor
    this->filename = fileName;
    if(filename.empty()) this->filename = makeDataPath(DEFAULT_FILENAME);
}

NvIpc::~NvIpc() {
    //dtor
}

void NvIpc::read() {
    ifstream m(filename, ios::in | ios::binary);
    mIpcread = readULong(m);
    mChecked = readULong(m);
    mSwitched = readULong(m);
    mCurrImgId = readUInt(m);
    mExitStatus =  static_cast<NVExitStatus>(readUChar(m));
    mPaused = readUChar(m);

    unsigned short sz = readUShort(m);
    if(sz > MAX_CMD_COUNT) throw invalid_argument("MAX COUNT EXCEEDED");
    IpcCommand ipcc(NVCommand::NOP);
    dq.clear();
    for(unsigned short x = 0; x < sz; x++) {
        m.read(reinterpret_cast<char *>(&ipcc), sizeof(IpcCommand));
        dq.push_back(ipcc);
    }
    m.close();
}

void NvIpc::write() {
    ofstream m(filename, ios::out | ios::binary);
    writeULong(m, mIpcread);
    writeULong(m, mChecked);
    writeULong(m, mSwitched);
    writeUInt(m, mCurrImgId);
    writeUChar(m, static_cast<unsigned short>(mExitStatus));
    writeUChar(m, static_cast<unsigned short>(mPaused));

    unsigned short sz = dq.size();
    //if(sz > 0) { cout << "writing " << sz << " commands" << endl; }
    writeUShort(m, sz);
    for(auto ipcc = dq.begin(); ipcc != dq.end(); ipcc++) {
        IpcCommand ipc = *ipcc;
        m.write(reinterpret_cast<char *>(&ipc), sizeof(IpcCommand));
    }
    m.close();
}

void NvIpc::push(IpcCommand cmd) {
    dq.push_back(cmd);
}

IpcCommand NvIpc::pop() {
    IpcCommand frnt;
    if(!dq.empty()) {
        frnt = dq.front();
        dq.pop_front();
    }
    return frnt;
}

void NvIpc::updateChecked() {
    mChecked = duration_cast<milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
}

void NvIpc::updateIpcRead() {
    mIpcread = duration_cast<milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
}

void NvIpc::updateSwitched(unsigned int imageId) {
    mSwitched = duration_cast<milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
    mCurrImgId = imageId;
}

void NvIpc::setRunning() {
    mExitStatus = NVExitStatus::RUNNING;
}

void NvIpc::setExit() {
    mExitStatus = NVExitStatus::EXIT;
    write();
}

void NvIpc::setExited() {
    mExitStatus = NVExitStatus::EXITED;
}

void NvIpc::nextImage() {
    read();
    push(IpcCommand(NVCommand::SWITCH_NOW));
    write();
}

void NvIpc::setPaused(bool paused) {
    read();
    mPaused = paused?1:0;
    write();
}

string NvIpc::debugString() {
    string s = "filename: ";
    s += filename;
    s += " exit: ";
    s += to_string(static_cast<int>(mExitStatus));
    s += " checked: ";
    s += to_string(mChecked);
    s += " switched: ";
    s += to_string(mSwitched);
    s += " paused: ";
    s += to_string(mPaused);
    s += " cmds: ";
    for(auto ipcc = dq.begin(); ipcc != dq.end(); ipcc++) {
        s += " [";
        s += to_string(static_cast<int>(ipcc->cmd));
        s += ":";
        s += to_string(ipcc->id);
        s += "] ";
    }
    return s;
}
