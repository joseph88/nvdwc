#include "nvsettings.h"

#include <string>
#include <fstream>
#include <filesystem>
#include "json.hpp"
#include "util.h"

using namespace std;
using json = nlohmann::json;

NvSettings::NvSettings() {
    //ctor
    this->filename = makeDataPath(DEFAULT_FILENAME);
    this->dataDir = getDefaultDataPath();
    this->desktop = GNOME;
}

NvSettings::~NvSettings() {
    //dtor
}


void NvSettings::save() {

    json j = {
        { "switchSeconds", switchSeconds },
        { "checkSeconds", checkSeconds },
        { "ipcMillis", ipcMillis },
        { "dataDir", dataDir },
        { "desktop", desktop },
        { "allPos", allPos },
        { "thumbSize", thumbSize },
        { "screenWidth", screenWidth },
        { "screenHeight", screenHeight }
    };

    ofstream m(filename, ios::out | ios::binary);
    m << j;
    m.close();
}

void NvSettings::load() {

    json j;

    if(filesystem::exists(filename))  {

        ifstream m(filename, ios::in | ios::binary);
        m >> j;
        m.close();

        try {
            switchSeconds = j["switchSeconds"];
        } catch(...) {}
        try {
            checkSeconds = j["checkSeconds"];
        } catch(...) {}
        try {
            ipcMillis = j["ipcMillis"];
        } catch(...) {}
        try {
            dataDir = j["dataDir"];
        } catch(...) {}
        try {
            desktop = j["desktop"];
        } catch(...) {}
        try {
            allPos = j["allPos"];
        } catch(...) {}
        try {
            thumbSize = j["thumbSize"];
        } catch(...) {}
        try {
            screenWidth = j["screenWidth"];
        } catch(...) {}
        try {
            screenHeight = j["screenHeight"];
        } catch(...) {}
        std::cout << debugString() << std::endl;
    } else {
        save();
    }
}

string NvSettings::getDataFilePath(string fileName) {
    string fp = dataDir;
    if(!ends_with(fp, "/")) fp += "/";
    fp += fileName;
    return fp;
}

string NvSettings::getThumbFilePath(unsigned int id) {
    string fp = dataDir;
    if(!ends_with(fp, "/")) fp += "/";
    fp += "thumbs/";
    fp += to_string(id);
    fp += DEFAULT_IMAGE_EXT;
    return fp;
}

string NvSettings::getImgFilePath(unsigned int id) {
    string fp = dataDir;
    if(!ends_with(fp, "/")) fp += "/";
    fp += "imgs/";
    fp += to_string(id);
    fp += DEFAULT_IMAGE_EXT;
    return fp;
}

string NvSettings::getCurrPath() {
    string fp = dataDir;
    if(!ends_with(fp, "/")) fp += "/";
    fp += "curr/";
    return fp;
}


string NvSettings::getCurrFilePath(unsigned int id) {
    string fp = dataDir;
    if(!ends_with(fp, "/")) fp += "/";
    fp += "curr/";
    fp += to_string(id);
    fp += DEFAULT_IMAGE_EXT;
    return fp;
}



string NvSettings::debugString() {
    json j = {
        { "switchSeconds", switchSeconds },
        { "checkSeconds", checkSeconds },
        { "ipcMillis", ipcMillis },
        { "dataDir", dataDir },
        { "desktop", desktop },
        { "allPos", allPos },
        { "thumbSize", thumbSize },
        { "screenWidth", screenWidth },
        { "screenHeight", screenHeight }
    };
    return j.dump();
}
