#include "imgpackdbidx.h"
#include "util.h"

using namespace std;

ImgPackDBIdx::ImgPackDBIdx() {
    setFilename(string());
}

ImgPackDBIdx::ImgPackDBIdx(string fileName) {
    //ctor
    setFilename(fileName);
}

ImgPackDBIdx::~ImgPackDBIdx() {
    //dtor
}

void ImgPackDBIdx::load() {
    ifstream m(filename, ios::in | ios::binary);
    m >> *this;
    m.close();
}

void ImgPackDBIdx::save() {
    ofstream m(filename, ios::out | ios::binary);
    m << *this;
    m.close();
}

void ImgPackDBIdx::setFilename(string fileName) {
    this->filename = fileName;
    if(this->filename.empty()) this->filename = makeDataPath(DEFAULT_FILENAME);
}

const std::set<unsigned int> ImgPackDBIdx::readForDoy(unsigned int doy) const {
    ifstream m(filename, ios::in | ios::binary);
    set<unsigned int> r;
    unsigned int doyOffset = doy * sizeof(unsigned int) + doy * sizeof(unsigned short);
    unsigned int offset = 0;
    unsigned short sz = 0;
    //cout << "seeking to pos: " << doyOffset << endl;
    //cout.flush();
    m.seekg(doyOffset);
    offset = readUInt(m);
    sz = readUShort(m);
    //cout << "offset: " << offset << " sz: " << sz << " doyOffset: " << doyOffset << endl;
    //cout.flush();
    // move to list
    m.seekg(offset, ios_base::beg);
    unsigned int iid;
    for(auto j = 0; j < sz; j++) {
        iid = readUInt(m);
        //cout << "   read " << iid << endl;
        r.insert(iid);
    }
    m.close();
    return r;
}

unsigned int ImgPackDBIdx::imgCount() {
    unsigned int cnt = 0;
    for(unsigned int x = 0; x < 366; x++) {
        cnt += days[x].size();
    }
    return cnt;
}

void ImgPackDBIdx::clear() {
    for(unsigned int x = 0; x < 366; x++) {
        days[x].clear();
    }
}

void ImgPackDBIdx::updateDoy(unsigned int iid, unsigned short doy) {
    if(doy < 366) days[doy].insert(iid);
}

string ImgPackDBIdx::debugString() {
    string s = "  ================  DB IDX  ====================\n";
    for(auto x = 0; x < 366; x++) {
        s += "[" + to_string(x) + "] ";
        set st = days[x];
        for(int i: st) {
            s += to_string(i) + ", ";
        }
        s += "\n";
    }
    return s;
}


std::ofstream &operator<<(std::ofstream &m, const ImgPackDBIdx &i) {
    unsigned int coff = 366 * sizeof(unsigned int) + 366 * sizeof(unsigned short);
    unsigned short sz = 0;
    for(auto x = 0; x < 366; x++) {
        writeUInt(m, coff);
        sz = i.days[x].size();
        writeUShort(m, sz);
        coff += sz * sizeof(unsigned int);
    }
    for(auto x = 0; x < 366; x++) {
        for(unsigned int iid: i.days[x]) {
            writeUInt(m, iid);
        }
    }
    return m;
}

std::ifstream &operator>>(std::ifstream &m, ImgPackDBIdx &i) {
    array<unsigned int, 366> iidx;
    array<unsigned short, 366> isz;
    for(auto x = 0; x < 366; x++) {
        iidx[x] = readUInt(m);
        isz[x] = readUShort(m);
    }
    for(auto x = 0; x < 366; x++) {
        unsigned short sz = isz[x];
        for(auto j = 0; j < sz; j++) {
            i.days[x].insert(readUInt(m));
        }
    }
    return m;
}
