#include <sstream>
#include <random>
#include <string>

unsigned int random_char() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 255);
    return dis(gen);
}

std::string random_hex_num() {
    const char* h = "0123456789abcdef";
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 15);
    return std::string(1, h[dis(gen)]);
}

std::string generate_hex(const unsigned int len) {
    std::stringstream ss;
    for (auto i = 0; i < len; i++) {
        const auto rc = random_char();
        std::stringstream hexstream;
        hexstream << std::hex << rc;
        auto hex = hexstream.str();
        ss << (hex.length() < 2 ? '0' + hex : hex);
    }
    return ss.str();
}

std::string newUUID() {
    std::string s = generate_hex(4);
    s += "-";
    s += generate_hex(2);
    s += "-4";
    s += random_hex_num();
    s += generate_hex(1);
    s += "-a";
    s += random_hex_num();
    s += generate_hex(1);
    s += "-";
    s += generate_hex(6);
    return s;
}
