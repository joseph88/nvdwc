#include <ctime>
#include <filesystem>
#include "util.h"

using namespace std;

void writeUChar(std::ofstream &m, const unsigned char& s) {
    m.write(reinterpret_cast<const char *>(&s), sizeof(unsigned char));
}

unsigned char readUChar(std::ifstream &m) {
    unsigned char c;
    m.read(reinterpret_cast<char *>(&c), sizeof(unsigned char));
    return c;
}

void writeUShort(ofstream &m, const unsigned short& s) {
    m.write(reinterpret_cast<const char *>(&s), sizeof(unsigned short));
}

unsigned short readUShort(std::ifstream& m) {
    unsigned short s;
    m.read(reinterpret_cast<char *>(&s), sizeof(unsigned short));
    return s;
}


void writeUInt(ofstream &m, const unsigned int& i) {
    m.write(reinterpret_cast<const char *>(&i), sizeof(unsigned int));
}

unsigned int readUInt(std::ifstream& m) {
    unsigned int i;
    m.read(reinterpret_cast<char *>(&i), sizeof(unsigned int));
    return i;
}

void writeULong(ofstream &m, const unsigned long& i) {
    m.write(reinterpret_cast<const char *>(&i), sizeof(unsigned long));
}

unsigned long readULong(std::ifstream& m) {
    unsigned long i;
    m.read(reinterpret_cast<char *>(&i), sizeof(unsigned long));
    return i;
}

void writeSString(ofstream &m, const string& s) {
    unsigned int len = s.length();
    if(len > MAX_SSTRING) throw invalid_argument("String was too long.");
    m.write(reinterpret_cast<const char *>(&len), sizeof(unsigned short));
    m.write(s.c_str(), len);
}

std::string readSString(std::ifstream& m) {
    unsigned short len;
    m.read(reinterpret_cast<char *>(&len), sizeof(unsigned short));
    if(len > MAX_SSTRING) throw invalid_argument("String was too long");
    char buffer[len];
    m.read(buffer, len);
    return string(buffer, len);
}

unsigned int getDoy() {
    time_t now = time(nullptr);
    return localtime(&now)->tm_yday + 1;
}

char readChar(ifstream& m) {
    char c;
    m.read(&c, 1);
    return c;
}

int readIntVB(ifstream &m) {
    int i = 0;
    char buff[20];
    buff[19] = '\0';
    char* c = buff;
    while(i < 19) {
        m.read(c, 1);
        if(*c == '|') {
            *c = '\0';
            break;
        }
        c++;
        i++;
    }
    cout << "converting: " << c << endl;
    return atoi(buff);
}

long readLongVB(ifstream &m) {
    int i = 0;
    char buff[20];
    char* c = buff;
    c[19] = '/0';
    while(i < 19) {
        m.read(c, 1);
        if(*c == '|') {
            *c = '\0';
            break;
        }
        c++;
    }
    return atol(buff);
}

string readStringVB(ifstream &m) {
    int i = 0;
    string s = "";
    char c = readChar(m);
    while(c != '|' && i < 500) {
        s += c;
        c = readChar(m);
        i++;
    }
    return s;
}

string makeDataPath(string fileName) {
    string dir = getenv("HOME");
    dir += "/.config/";
    dir += APP_ID;
    dir += "/";
    filesystem::create_directories(filesystem::path(dir));
    dir += fileName;
    return dir;
}

string getDefaultDataPath() {
    string dir = getenv("HOME");
    dir += "/.config/";
    dir += APP_ID;
    dir += "/";
    return dir;
}

string getDoyMonth(unsigned int doy) {
    if(doy < 31) return "Jan";
    else if (doy < 60) return "Feb";
    else if (doy < 91) return "Mar";
    else if (doy < 121) return "Apr";
    else if (doy < 152) return "May";
    else if (doy < 182) return "Jun";
    else if (doy < 213) return "Jul";
    else if (doy < 244) return "Aug";
    else if (doy < 274) return "Sep";
    else if (doy < 305) return "Oct";
    else if (doy < 335) return "Nov";
    else return "Dec";
}

unsigned short getDoyDay(unsigned int doy) {
    if(doy < 31) return doy;
    else if (doy < 60) return doy - 31;
    else if (doy < 91) return doy - 60;
    else if (doy < 121) return doy - 91;
    else if (doy < 152) return doy - 121;
    else if (doy < 182) return doy - 152;
    else if (doy < 213) return doy - 182;
    else if (doy < 244) return doy - 213;
    else if (doy < 274) return doy - 244;
    else if (doy < 305) return doy - 274;
    else if (doy < 335) return doy - 305;
    else return doy - 335;
}

string getDoyDate(unsigned int doy) {
    string s = getDoyMonth(doy);
    s += " ";
    s += to_string(getDoyDay(doy)+1);
    return s;
}
